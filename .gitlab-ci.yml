#------------------------------------------------------------
# Stages
#------------------------------------------------------------
stages:
  - test
  - publish


#------------------------------------------------------------
# Multi Environment Definition
#------------------------------------------------------------
.multi:env:matrix:
  parallel:
    matrix:
      - IPB_ENV: [dev, test, prod]


#------------------------------------------------------------
# Variables
#------------------------------------------------------------
# gobal variables
variables:
  # gitlab settings: global
  APP_NAME: "IPB-App"
  TAG_LATEST: "latest"
  IMAGE_TAG_SHA: $CI_REGISTRY_IMAGE/$APP_NAME:$CI_COMMIT_SHORT_SHA
  IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE/$APP_NAME:$TAG_LATEST
  # gcp settings: global
  POOL_ID: "gitlab-cicd"
  PROVIDER_ID: "gitlab-cicd"                            
  ACCESS_TOKEN: ""

# variables dependent on environment in scriptale manner
.multi:env:var:
  variables:
    PROJECT_ID: "my-project-${IPB_ENV}"
    SERVICE_ACCOUNT_EMAIL: "gitlab-cicd-identity@my-fancy-project-${IPB_ENV}.iam.gserviceaccount.com"

# variables dependent on environment with manual settings
.multi:env:var:lab:
  variables:
    PROJECT_NUMBER: "123"

.multi:env:var:dev:
  variables:
    PROJECT_NUMBER: "234"

.multi:env:var:test:
  variables:
    PROJECT_NUMBER: "345"

.multi:env:var:prod:
  variables:
    PROJECT_NUMBER: "456"


#------------------------------------------------------------
# Multi-Environment Parent Job
#------------------------------------------------------------

#------------------------------------------------------------
#  !!!!!!!!!!!!
#  !!! NOTE !!!
#  !!!!!!!!!!!!
#  > Gitlab allows only one level of reference section inclusion
#  > We we use !reference in the following job
#  > Hence, this job CANNOT be included with !reference again
#  > Instead, include this job as parent job
#------------------------------------------------------------
# set pipeline execution rules for $IPB_ENV values and commit branches
.multi:env:
  variables:
    !reference [.multi:env:var, variables]
  parallel: 
    !reference [.multi:env:matrix, parallel]
  rules:
    #-------------------------
    # rules for dev
    #-------------------------
    - if: ($IPB_ENV == "dev") && ($CI_COMMIT_BRANCH == "main")
      variables:
        !reference [.multi:env:var:dev, variables]
      when: always
    - if: ($IPB_ENV == "dev") && ($CI_COMMIT_BRANCH != "main")
      variables:
        !reference [.multi:env:var:dev, variables]
      when: manual
    #-------------------------
    # rules for test
    #-------------------------
    - if: ($IPB_ENV == "test") && ($CI_COMMIT_BRANCH == "stable")
      variables:
        !reference [.multi:env:var:test, variables]
      when: manual
    #-------------------------
    # rules for prod
    #-------------------------
    - if: ($ENV == "prod") && ($CI_COMMIT_BRANCH == "stable")
      variables:
        !reference [.multi:env:var:prod, variables]
      when: manual
    #-------------------------
    # rules for excluding test and prod
    #-------------------------
    - if: ($ENV == "test" || $ENV == "prod") && ($CI_COMMIT_BRANCH != "stable")
      when: never
  

#------------------------------------------------------------
# Authentication Jobs
#------------------------------------------------------------
# perform gcloud auth via glcoud
.gcloud:auth:iam:
  before_script:
    #------------------------------------------------------------
    # STEP 1:
    #   -> Save the GitLab JWT token ${CI_JOB_JWT_V2} to a local file
    #   -> Create a credential file from the workload identity pool
    # DOCU
    #   -> Gitlab Docu: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
    #   -> GCP Process docu: https://cloud.google.com/iam/docs/using-workload-identity-federation#generate-automatic
    #   -> GCP API docu: https://cloud.google.com/sdk/gcloud/reference/iam/workload-identity-pools/create-cred-config
    #------------------------------------------------------------
    - echo ${CI_JOB_JWT_V2} > .cicd_jwt_file
    - |
      gcloud iam workload-identity-pools create-cred-config \
      projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/providers/${PROVIDER_ID} \
      --service-account=${SERVICE_ACCOUNT_EMAIL}  \
      --service-account-token-lifetime-seconds=900 \
      --output-file=.gcp_cred.json \
      --credential-source-file=.cicd_jwt_file
    #------------------------------------------------------------
    # STEP 2:
    #   -> Set the project and the service account
    #   -> Set the default credentials
    #   -> login
    # NOTE: 
    #   -> keep this order, since it processes 
    #   -> exactly as glcoud init would do it
    # DOCU
    #   -> Gitlab Docu: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
    #   -> GCP Default Credentials: https://cloud.google.com/docs/authentication/application-default-credentials#GAC
    #   -> Gcloud auth docu: https://cloud.google.com/sdk/docs/authorizing
    #   -> GCP API docu: https://cloud.google.com/sdk/gcloud/reference/auth/login
    #------------------------------------------------------------
    - gcloud auth login ${SERVICE_ACCOUNT_EMAIL} --cred-file=`pwd`/.gcp_cred.json --project=${PROJECT_ID}
    - gcloud config set account ${SERVICE_ACCOUNT_EMAIL}
    - gcloud config set project ${PROJECT_ID}
    - export GOOGLE_APPLICATION_CREDENTIALS=`pwd`/.gcp_cred.json


#------------------------------------------------------------
# stage: test
#------------------------------------------------------------
#lint dockerfile
lint:dockerfile:
  stage: test
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint Dockerfile
  allow_failure: true


# test if python app starts up
test:python:app:start:
  stage: test
  image: debian:11
  script:
    #start fastapi app
    - apt-get update && apt-get install -y python3.9 python3-pip curl
    - python3 -m pip install --upgrade pip
    - pip cache purge
    - pip install --no-cache-dir  -r requirements.txt
    - python3 lib/main.py &
    # save pid and call app
    - export APP_PID=$!
    - sleep 2
    - curl http://0.0.0.0:8000/docs
    # kill app 
    - sleep 5
    - kill ${APP_PID}


#test google api access
test:google:api:access:
  stage: test
  extends: .multi:env
  image:
    name: gcr.io/google.com/cloudsdktool/google-cloud-cli:slim                   
  before_script:
    - apt-get install -y jq
    - !reference [.gcloud:auth:iam, before_script]
  script:
    - echo "[SUCCESS] Login settings are:"
    - gcloud auth list
    - gcloud config list --all


# build docker image and run container --> test only 
docker:build:run:test:
  stage: test
  image:
    name: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    # login
    - mkdir -p $HOME/.docker/
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_DEPLOY_USER:$CI_DEPLOY_PASSWORD | base64 | tr -d '\n')\"}}}" > $HOME/.docker/config.json
  script:
    - apk add curl
    # build and run the new image with tag = short commit sha
    - docker build -t $IMAGE_TAG_SHA .
    - docker run -p 8000:8000 --rm --env-file .env --name tmp-ctr $IMAGE_TAG_SHA &
    # save pid and call app
    - export PROC_PID=$!
    - sleep 2
    - curl http://0.0.0.0:8000/docs
    # kill process
    - sleep 5
    - kill ${PROC_PID}


#------------------------------------------------------------
# stage: publish
#------------------------------------------------------------
publish:app:
  stage: publish
  extends: .multi:env
  image:
    name: gcr.io/google.com/cloudsdktool/google-cloud-cli:slim
  before_script:
    # login to GCP
    - apt-get install -y jq
    - !reference [.gcloud:auth:iam, before_script]
  script:
    # install yq
    - apt-get install -y wget tar
    - wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    # merge app yaml files
    - cp deploy/base/app.yaml app.yaml
    - yq eval-all --inplace 'select(fileIndex == 0) * select(fileIndex == 1)' app.yaml deploy/${IPB_ENV}/app-patch.yaml
    # deploy
    - gcloud app deploy --project $PROJECT_ID
