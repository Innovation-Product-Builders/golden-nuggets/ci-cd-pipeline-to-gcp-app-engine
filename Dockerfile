FROM debian:11

# install python3 and pip
RUN apt-get update && apt-get install -y python3.9 python3-pip
RUN python3 -m pip install --upgrade pip
RUN pip cache purge

# change to non-root user in image
ENV HOME=/home/worker
RUN adduser --home ${HOME} --disabled-password worker
RUN chown worker:worker ${HOME}
USER worker

# Set the working directory
WORKDIR ${HOME}

# copy requirement file & update python environment
COPY requirements.txt ${HOME}/requirements.txt
RUN pip install --no-cache-dir -r ${HOME}/requirements.txt

# copy files
COPY ./lib ${HOME}/lib
ENV PYTHONPATH="${HOME}/lib:${PYTHONPATH}"

CMD ["python3", "lib/main.py"]
