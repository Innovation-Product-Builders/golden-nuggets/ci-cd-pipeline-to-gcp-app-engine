# How to build a multi-environment CI/CD Pipeline from GitLab to GCP App Engine

## Introduction
GitLab CI/CD is a powerful tool for automating the software development process, from building and testing code to deploying it to various environments. One of the many options for deploying code is to use GCP's App Engine, a fully managed platform for building and deploying web applications. In this article, we will show you how to set up a GitLab CI/CD pipeline to deploy a simple web application to App Engine, using the `CI_JOB_JWT_V2` token for authentication.

## General Setup
Before you begin, you will need to have a 
- GCP account and a project set up, 
- as well as a GitLab account and a repository for your application. 
- You will also need to have the `gcloud` command-line tool installed on your local machine.

## GCP Setup

### Service Account
When we setup a CI/CD pipeline to push to GCP App Engine, we need to be able to login from the Ci/CD pipeline to GCP. For this, we need to create a service account in GCP which we can use in the CI/CD pipeline. This means, we will operate within the pipeline under identity of this service account workload identity pool for short-lived credentials. To accomplish this, you have to go GCP IAM (IAM =Identity and Access Management) and you have to create a service account there. Since we use this
service account within our GitLab CI/CD pipeline, we could name it `gitlab-cicd-identity`.

### Authentication for service accounts
The standard way to login to GCP with a service account is to download the service account credentials in a key file. However, to save locally or in repository long-lived credentials for a service account are considered to be bad practice. 

It is better to choose an approach that allows us to use short-lived credentials only. This can be done in GCP IAM with so-called workload identity pools.
For these reasons, we create under `GCP IAM > Workload Identity Federation` an workload identity pool, e.g., with the name `gitlab-cicd`.

Next, we go in this workload pool, and we grant our `gitlab-cicd-identity` service account access to it. At the same time, we need to assign to our service account the
role `Workload Identity User`, such that the service is allowed to use a workload identity pool.

The last step is that we need to setup in our identity pool `gitlab-cicd` and identity provider, which is GitLab in our case. This is done by clicking on `add provider`
and setting up an OIDC provider. In the following dialogue, we have to specify:
- the name: `gitlab-cicd`. I.e., we choose the same name for the identity provider as we have it already for the pool.
- the issuer url: https://gitlab.com/
- the allowed audience: https://gitlab.com
- the attribute mapping
  - attribute.aud  ~ assertion.aud
  - google.subject ~ assertion.sub
  - attribute.user_email ~ assertion.user_email
  - attribute.iss ~ assertion.iss

Then we click on save and we have our workload identity pool in which GitLab is configured as identity provider and where we have added our service account,
which we use to push from the CI/CD pipeline to GCP.

Further documentation about this can be found here:
- Gitlab Docu: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
- GCP Docu: https://cloud.google.com/iam/docs/configuring-workforce-identity-federation
- GCP Docu: https://cloud.google.com/iam/docs/workload-identity-federation-with-deployment-pipelines#create_the_workload_identity_pool_and_provider


## App Engine Setup in your repository
The first step is to create a file called `app.yaml` in the root of your repository. This file tells App Engine how to configure and run your application. 
However, remember that we are deploying to multiple GCP projects (dev, test, prod) out of one pipeline. So we need to configure your `app.yaml` file 
automatically to the correct project automatically. In order to do so, we define in our project the folder structure:
```
|repository
|--deploy
|----base
|------app.yaml
|----dev
|------app-patch.yaml
|----test
|------app-patch.yaml
|----prod
|------app-patch.yaml
```
This is the well-known base-overlay approach from `kustomize` when the deployment goes to a Kubernetes cluster. Since we deploy to App Engine, we have to mimic this behavior on our own:
- In the base folder we put our `app.yaml` file with those parts that remain the same for our dev, test, prod GCP project.
- In the overlay foldersdev, test,prod, we have the `app-patch.yaml` file that contains the environment specific settings.

Next, we install the command line tool `yq` which is able to merge two yaml files `file1.yaml` and `file1-patch.yaml` into `file1.yaml`
with the command
```
yq eval-all --inplace 'select(fileIndex == 0) * select(fileIndex == 1)' file1.yaml file1-patch.yaml
```
You will see how this  in our CI/CD pipeline in the very end of this article.

## CI/CD Pipeline Setup
Next, you will need to create a `.gitlab-ci.yml` file in the root of your repository. This file tells GitLab how to build and test your application, as well as how to deploy it to App Engine. Now we go step-by-step through the `.gitlab-ci.yml` file to deploy to App Engine in your dev, test, and prod GCP projects.

## The stages
First, define the stages of your pipeline by
```
#------------------------------------------------------------
# Stages
#------------------------------------------------------------
stages:
  - test
  - publish
```

## The multi-environment definition
Next, we configure our pipeline such that the same job can run in parallel for our different GCP projects. This is done via:
```
#------------------------------------------------------------
# Multi Environment Definition
#------------------------------------------------------------
.multi:env:matrix:
  parallel:
    matrix:
      - IPB_ENV: [dev, test, prod]
```
This will generate three parallel version of the same job and the variable `IPB_ENV`, will take different one of the defined values in the parallel jobs.

## The global variables
To configure the behavior of our pipeline, we can globally define variables that are available for each job in our `.gitlab-ci.yaml` file
```
#------------------------------------------------------------
# Variables
#------------------------------------------------------------
# global variables
variables:
  # gitlab settings: global
  APP_NAME: "IPB-App"
  TAG_LATEST: "latest"
  IMAGE_TAG_SHA: $CI_REGISTRY_IMAGE/$APP_NAME:$CI_COMMIT_SHORT_SHA
  IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE/$APP_NAME:$TAG_LATEST
  # gcp settings: global
  POOL_ID: "gitlab-cicd"
  PROVIDER_ID: "gitlab-cicd"                            
  ACCESS_TOKEN: ""
```

The next set of global variables will take different values in each of the parallel jobs, i.e., they depend on the values of the variable `IPB_ENV`. 
Note that we assume here that you have to have
- defined the name of the GCP projects 
- and the used service account, in our example `gitlab-cicd-identity`, 
such that **they follow the same naming pattern and only differ by the environment value**
```
# variables dependent on environment in scriptable manner
.multi:env:var:
  variables:
    PROJECT_ID: "my-project-${IPB_ENV}"
    SERVICE_ACCOUNT_EMAIL: "gitlab-cicd-identity@my-fancy-project-${IPB_ENV}.iam.gserviceaccount.com"
```
However, there are also variables, such as the GCP project numbers that need to hard-coded for each of environments manually
```
# variables dependent on environment with manual settings
.multi:env:var:dev:
  variables:
    PROJECT_NUMBER: "123"

.multi:env:var:test:
  variables:
    PROJECT_NUMBER: "234"

.multi:env:var:prod:
  variables:
    PROJECT_NUMBER: "345"
```

## The multi-environment parent job
Since GitLab allows inheritance of jobs, we define a parent job that holds all the common multi-environment setting that are needed in the jobs that we define later on.
We put the common multi-environment settings into a common parent job to avoid boilerplate code.

### Note on the !reference command
What is important to note here is that we use the `!reference` command which allows to reference only specific parts of a job in another job. This is another way of reusing common blocks throughout the pipeline. This approach is following more the composite pattern than using inheritance. However, when you use 
the `!reference` command be aware that it is possible only to reference once. So a part of a job that contains the `!reference` command cannot be included
with the `!reference` command again. Here you need to choose job inheritance for reusage.

Since we use in the following parent job `!reference` commands to include the previously defined global settings, we can only reuse the following job as a parent job.

### Definitions
The definitions in the following job are:
- to include the variables
- to make the job parallel
- to define execution rules for th different environments

In particular, our execution rules are such that:
- we can push to dev automatically from the `main` branch
- we can push manually to dev from every feature branch
- we can only push to test and prod from a `stable` branch

Since GitLab allows not for nested if statements, the code for going through the described cases is very clumsy. Note that we control the type of
execution by the 
```
when: always/manual/never
``` 
statement and note that we include in every if statement also the correct global variables for our GCP dev, test, prod projects.
```
#------------------------------------------------------------
# Multi-Environment Parent Job
#------------------------------------------------------------

# set pipeline execution rules for $IPB_ENV values and commit branches
.multi:env:
  variables:
    !reference [.multi:env:var, variables]
  parallel: 
    !reference [.multi:env:matrix, parallel]
  rules:
    #-------------------------
    # rules for dev
    #-------------------------
    - if: ($IPB_ENV == "dev") && ($CI_COMMIT_BRANCH == "main")
      variables:
        !reference [.multi:env:var:dev, variables]
      when: always
    - if: ($IPB_ENV == "dev") && ($CI_COMMIT_BRANCH != "main")
      variables:
        !reference [.multi:env:var:dev, variables]
      when: manual
    #-------------------------
    # rules for test
    #-------------------------
    - if: ($IPB_ENV == "test") && ($CI_COMMIT_BRANCH == "stable")
      variables:
        !reference [.multi:env:var:test, variables]
      when: manual
    #-------------------------
    # rules for prod
    #-------------------------
    - if: ($IPB_ENV == "prod") && ($CI_COMMIT_BRANCH == "stable")
      variables:
        !reference [.multi:env:var:prod, variables]
      when: manual
    #-------------------------
    # rules for excluding test and prod
    #-------------------------
    - if: ($IPB_ENV== "test" || $IPB_ENV == "prod") && ($CI_COMMIT_BRANCH != "stable")
      when: never
```  

## GCP Authentication
Next, we define the job to authenticate to GCP with the `CI_JOB_JWT_V2` token and our previously created identity pool in GCP IAM.
First, we save the GitLab JWT token `CI_JOB_JWT_V2` to a local file `.cicd_jwt_file` and then we create in a rather long command
a credential file from the workload identity pool. Note that in this command we use the previously defined variables `POOL_ID`, 
`PROVIDER_ID`, and `SERVICE_ACCOUNT_EMAIL`, which need to match the settings of our workload identity pool. Furthermore, we set the 
lifetime of the short-lived credentials to 15 min = 900 sec.

Further documentation about this step can be found here:
- Gitlab Docu: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
- GCP Process docu: https://cloud.google.com/iam/docs/using-workload-identity-federation#generate-automatic
- GCP API docu: https://cloud.google.com/sdk/gcloud/reference/iam/workload-identity-pools/create-cred-config
```
#------------------------------------------------------------
# Authentication Jobs
#------------------------------------------------------------
# perform gcloud auth via glcoud
.gcloud:auth:iam:
  before_script:
    # STEP 1:
    - echo ${CI_JOB_JWT_V2} > .cicd_jwt_file
    - |
      gcloud iam workload-identity-pools create-cred-config \
      projects/${PROJECT_NUMBER}/locations/global/workloadIdentityPools/${POOL_ID}/providers/${PROVIDER_ID} \
      --service-account=${SERVICE_ACCOUNT_EMAIL}  \
      --service-account-token-lifetime-seconds=900 \
      --output-file=.gcp_cred.json \
      --credential-source-file=.cicd_jwt_file
    # STEP 2:
    - gcloud auth login ${SERVICE_ACCOUNT_EMAIL} --cred-file=`pwd`/.gcp_cred.json --project=${PROJECT_ID}
    - gcloud config set account ${SERVICE_ACCOUNT_EMAIL}
    - gcloud config set project ${PROJECT_ID}
    - export GOOGLE_APPLICATION_CREDENTIALS=`pwd`/.gcp_cred.json
```    
In step 2 of the authentication job, we login to our GCP project such that we are able to deploy to GCP App Engine later on.
Further documentation about this step can be found here:
- Gitlab Docu: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/
- GCP Default Credentials: https://cloud.google.com/docs/authentication/application-default-credentials#GAC
- gloud auth docu: https://cloud.google.com/sdk/docs/authorizing
- GCP API docu: https://cloud.google.com/sdk/gcloud/reference/auth/login

Note that we have defined all of the above commands in the `before_script` section such that every child job has this logic running before the `script` section in the child job gets executed.

## Stage test
Next, we define some test jobs to check our code before we deploy it. 

### Test dockerfile syntax
First of all, we check the syntax of our Dockerfile with
```
#lint dockerfile
lint:dockerfile:
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint Dockerfile
  allow_failure: true
```

### Test python app
Second, we test if the plain python FastAPI app is runnable:
```
# test if python app starts up
test:python:app:start:
  image: debian:11
  script:
    #start fastapi app
    - apt-get update && apt-get install -y python3.9 python3-pip curl
    - python3 -m pip install --upgrade pip
    - pip cache purge
    - pip install --no-cache-dir -r requirements.txt
    - python3 lib/main.py &
    # save pid and call app
    - export APP_PID=$!
    - sleep 2
    - curl http://0.0.0.0:8080/docs
    # kill app 
    - sleep 5
    - kill ${APP_PID}
```

### Test GCP authentication
Third, we test if our GCP authentication job is working well and if we can really login to GCP. Note that we include our GCP Authentication job not via inheritance but
through the composite pattern style with `!reference`.
```
#test google api access
test:google:api:access:
  stage: test
  extends: .multi:env
  image:
    name: gcr.io/google.com/cloudsdktool/google-cloud-cli:slim                   
  before_script:
    - apt-get install -y jq
    - !reference [.gcloud:auth:iam, before_script]
  script:
    - echo "[SUCCESS] Login settings are:"
    - gcloud auth list
    - gcloud config list --all
```

### Test docker build and run
Fourth, since we deploy our app in a docker container to GCP App Engine, we test if building the Dockerfile and running it is successful.
In our example, you see in the `docker run` command that we have the app running on port 8080 and that we pass our environment setting
to the container via a `.env` file. Change this accordingly to your setup.
```
# build docker image and run container --> test only 
docker:build:run:test:
  stage: build
  image:
    name: docker:latest
  services:
    - docker:dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    # login
    - mkdir -p $HOME/.docker/
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_DEPLOY_USER:$CI_DEPLOY_PASSWORD | base64 | tr -d '\n')\"}}}" > $HOME/.docker/config.json
  script:
    - apk add curl
    # build and run the new image with tag = short commit sha
    - docker build -t $IMAGE_TAG_SHA .
    - docker run -p 8080:8080 --rm --env-file .env --name tmp-ctr $IMAGE_TAG_SHA &
    # save pid and call app
    - export PROC_PID=$!
    - sleep 2
    - curl http://0.0.0.0:8080/docs
    # kill process
    - sleep 5
    - kill ${PROC_PID}
```

## Publish to GCP App Engine
Once all tests are passed, we can finally deploy to GCP App Engine. However, remember that we are deploying to multiple GCP projects out of one pipeline.
So we need to configure your `app.yaml` file automatically to the correct project automatically, which we have already explained above. So, we are now well equipped to finally deploy our app the following job
```
#------------------------------------------------------------
# stage: publish
#------------------------------------------------------------
publish:app:
  stage: publish
  extends: .multi:env
  image:
    name: gcr.io/google.com/cloudsdktool/google-cloud-cli:slim
  before_script:
    # login to GCP
    - apt-get install -y jq
    - !reference [.gcloud:auth:iam, before_script]
  script:
    # install yq
    - apt-get install -y wget tar
    - wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
    # merge app yaml files
    - cp deploy/base/app.yaml app.yaml
    - yq eval-all --inplace 'select(fileIndex == 0) * select(fileIndex == 1)' app.yaml deploy/${IPB_ENV}/app-patch.yaml
    # deploy
    - gcloud app deploy --project $PROJECT_ID
```

## Note on required GCP roles
Note that we operate in this job under the identity of the service account for which we have successfully in the command `!reference [.gcloud:auth:iam, before_script]`.
Hence, this service account needs to have the required permissions to authenticate through our workload identity pool and deploy our app to GCP App Engine. 
So, you need to give this service account in GCP IAM the roles:
- App Engine Deployer
- App Engine Service Admin
- Cloud Build Editor
- Service Account User
- Storage Object Creator
- Storage Object Viewer
- Workload Identity User

## Summary
In this article, we walked you step-by-step through all required steps in GCP and GitLab to deploy your dockerized app to several GCP App Engine projects out of one CI/CD pipeline.
